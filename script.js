var notes = new Array();

var importantField = document.getElementById('importantfield');
var simpleField = document.getElementById('simplefield');
var completeImportantField = document.getElementById('compimportantfield');
var completeSimpleField = document.getElementById('compsimplefield');
var getError = document.getElementById('error');
var getError2 = document.getElementById('error2');
var addBlock = document.getElementById('add');
var mainBlock = document.getElementById('main');
var ok = document.getElementById('ok');
var ok2 = document.getElementById('ok2');

ok.onclick = function ok(){
    getError.style.display = "none";
    addBlock.style.display = "flex";
    mainBlock.style.display = "block";
};
ok2.onclick = function ok2(){
    getError2.style.display = "none";
    addBlock.style.display = "flex";
    mainBlock.style.display = "block";
};

var cycle1 = 0;
var cycle2 = 0;
var cycle3 = 0;

send.onclick = function test(){
 

    var noteValue = document.getElementById('note').value; 
    var par = document.createElement('p');
    var importantLabel = document.createElement('label');
    var simpleLabel = document.createElement('label');

    var doAsImportant = document.createElement('input');
    doAsImportant.setAttribute("type", "checkbox");
    var doAsImportantLabel = document.createElement('label');
    doAsImportantLabel.innerHTML = 'отметить как важное';


    //ЦИКЛ

    cycle1 = cycle1 + 1;
    cycle2 = cycle2 + 1;
    cycle3 = cycle3 + 1;


    par.innerHTML = noteValue;
    var importantInput = document.createElement('input'); //
    var simpleInput = document.createElement('input'); //

//

    importantInput.setAttribute("type", "checkbox");
    simpleInput.setAttribute("type", "checkbox");
    importantInput.id = 'importantcheck' + cycle2;
    simpleInput.id = 'simplecheck' + cycle3;
    importantLabel.innerHTML = "отметить как выполненное";
    simpleLabel.innerHTML = "отметить как выполненное";
    importantLabel.setAttribute("for", "importantcheck" + cycle2);
    simpleLabel.setAttribute("for", "simplecheck" + cycle3);


    var getDate = new Date();
    var day = getDate.getDate();
    var month = getDate.getMonth() + 1;
    var year = getDate.getFullYear();
    var hours = getDate.getHours();
    var minutes = getDate.getMinutes();
    var time = (day + "-" + month + "-" + year + " " + hours + ":" + minutes);
    var date = document.createElement('p');
    date.id = 'date';



// emptyfield 

    var doImportant = true;

    errors();
    function errors(){
    var importantCheckbox = document.getElementById('imp');
        if ( noteValue == "") {
            getError.style.display = "flex";
            addBlock.style.display = "none";
            mainBlock.style.display = "none";
            importantCheckbox.checked = false;
        } else if (notes.includes(noteValue) == true ) {
            getError2.style.display = "flex";
            addBlock.style.display = "none";
            mainBlock.style.display = "none"
            importantCheckbox.checked = false;
        }
        else if (importantCheckbox.checked && notes.includes(noteValue) == false && noteValue !== "") {
            notes.push(noteValue);
            console.log(notes);            
            importantField.appendChild(par);
            date.innerHTML = time;
            par.appendChild(date);
            par.id = 'importantnote';
            par.appendChild(importantInput);
            par.appendChild(importantLabel);
            par.appendChild(doAsImportant);
            doImportant = false;
            doAsImportant.checked = true;
            par.appendChild(doAsImportantLabel);
            doAsImportant.id = 'doasimportant' + cycle1;
            doAsImportantLabel.setAttribute("for","doasimportant" + cycle1);
            importantCheckbox.checked = false;
        }
        else if (noteValue !== "" && notes.includes(noteValue) == false) {
            notes.push(noteValue);
            console.log(notes);
            simpleField.appendChild(par);
            date.innerHTML = time;
            par.appendChild(date);
            par.id = 'simplenote';
            par.appendChild(simpleInput); //
            par.appendChild(simpleLabel);
            par.appendChild(doAsImportant);
            par.appendChild(doAsImportantLabel);
            doAsImportant.id = 'doasimportant' + cycle1;
            doAsImportantLabel.setAttribute("for","doasimportant" + cycle1);
        }
    };
    var reset = document.getElementById('note');
    reset.value = "";


    var getImportantCheck = document.getElementById('importantcheck');
    var getSimpleCheck = document.getElementById('simplecheck');

    var importantChecked = true;
    var simpleChecked = true;


    doAsImportant.onclick = function(){
        if (doImportant == true) {
            importantField.appendChild(par);
            par.id = 'importantnote';
            par.replaceChild(importantInput, simpleInput);
            par.replaceChild(importantLabel, simpleLabel);
            doImportant = false;
            console.log(importantChecked);
        } else {
            simpleField.appendChild(par);
            par.id = 'simplenote';
            par.replaceChild(simpleInput, importantInput);
            par.replaceChild(simpleLabel, importantLabel);

            doImportant = true;
        }
    };

    importantInput.onclick = function (){
        if (importantChecked == true) {
            completeImportantField.appendChild(par);
            par.id = 'completenote';
            doAsImportant.style.display = 'none';
            doAsImportantLabel.style.display = 'none';
            importantChecked = false;
            console.log(importantChecked);
        } else {
            importantField.appendChild(par);
            par.id = 'importantnote';
            doAsImportant.style.display = 'block';
            doAsImportantLabel.style.display = 'block';
            importantChecked = true;
        }
    };

    simpleInput.onclick = function (){
        if (simpleChecked == true) {
            completeSimpleField.appendChild(par);
            par.id = 'completenote';
            doAsImportant.style.display = 'none';
            doAsImportantLabel.style.display = 'none';
            simpleChecked = false;
        } else {
            simpleField.appendChild(par);
            par.id = 'simplenote';
            doAsImportant.style.display = 'block';
            doAsImportantLabel.style.display = 'block';
            simpleChecked = true;
        }
    };

};

//filter

var all = document.getElementById('all');
var important = document.getElementById('important');
var active = document.getElementById('active');
var unactive = document.getElementById('unactive');

all.onclick = function allFilter(){
    if (all.checked) {
        importantField.style.display="block";
        simpleField.style.display="block";
        completeImportantField.style.display="block";
        completeSimpleField.style.display="block";
    }
};

important.onclick = function importantFilter(){
    if (important.checked) {
        importantField.style.display="block";
        simpleField.style.display = "none";
        completeImportantField.style.display="block";
        completeSimpleField.style.display="none";
    }
};

active.onclick = function activeFilter(){
    if (active.checked) {
        importantField.style.display="block";
        simpleField.style.display="block";
        completeImportantField.style.display="none";
        completeSimpleField.style.display="none";
    }
};

unactive.onclick = function unactiveFilter(){
    if (unactive.checked) {
        importantField.style.display="none";
        simpleField.style.display="none";
        completeImportantField.style.display="block";
        completeSimpleField.style.display="block";
    }
};


